from twelvedays import *

song = TwelveDays()
day = 1
days = 12
print(song.getFirstLine(1))
print(song.getGift(1))

while day < days:
    day += 1
    verse = song.getFirstLine(day)
    n = day
    while n > 1:
        verse += song.getGift(n)
        n -= 1
    verse += 'And '
    verse += song.getGift(1)
    verse += '\n'
    print(verse)
